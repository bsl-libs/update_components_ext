﻿// Этап обновления компонента подсистемы.
//
// BSLLS:NestedFunctionInParameters-off - без лишних переменных выполнение быстрее
// BSLLS:EmptyRegion-off - пустые области разрешены

#Область ПрограммныйИнтерфейс

// Возвращает новое описание версии подсистемы
//
// Параметры:
// 	Компонент - Структура - Компонент подсистемы - владелец версии
// 	Версия - Строка - Номер обновляемой версии
//
// Возвращаемое значение:
// 	Структура
//
Функция Добавить(Компонент, Версия) Экспорт
	
	Результат	= Новый Структура();
	
	Версия(Результат, Версия);
	Обработчики(Результат, Новый Массив());
	
	пно_Компонент.Этапы(Компонент).Добавить(Результат);
	
	Возврат Результат;
	
КонецФункции // Добавить 

// Устанавливает значение номера версии. Возвращает значение на 
// 	момент до выполнения метода
//
// Параметры:
// 	Версия - Структура - Настройка обновления версии
// 	Знаечние - Строка - Новое значение номера версии
//
// Возвращаемое значение:
// 	Строка
//
Функция Версия(Этап, Значение = Null) Экспорт
	
	Возврат пно_Коллекция.СтруктураСвойство(Этап, "Версия", Значение);
	
КонецФункции // Номер 

// Устанавливает набор обработчиков этапа обновления. Возвращает значение на
// 	момент до вызова метода
//
// Параметры:
// 	Этап - Структура - Модифицируемый этап обновления
// 	Значение - Массив - Новый набор обработчиков
//
// Возвращаемое значение:
// 	Массив
//
Функция Обработчики(Этап, Значение = Null) Экспорт
	
	Возврат пно_Коллекция.СтруктураСвойство(Этап, "Обработчики", Значение);
	
КонецФункции // Обработчики  

#КонецОбласти

#Область СлужебныйПрограммныйИнтерфейс

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

#КонецОбласти

// BSLLS:NestedFunctionInParameters-on
// BSLLS:EmptyRegion-on
 