﻿// Обслеживание служебной информации о процессе обновления
//
// BSLLS:NestedFunctionInParameters-off - без лишних переменных выполнение быстрее
// BSLLS:EmptyRegion-off - пустые области разрешены

#Область ПрограммныйИнтерфейс

#КонецОбласти

#Область СлужебныйПрограммныйИнтерфейс

// Устанавливает признак завершения процесса обновления для типа данных
//
// Параметры:
// 	Процесс - Структура - Информация о процессе обновления, код взят из типовых обработчиков обновлений
// 	ТипИмя - Строка - Полное имя типа обновляемых данных
//
Процедура ЗавершенУстановить(Процесс, ТипИмя) Экспорт
	
	Если Процесс <> Неопределено Тогда
		Процесс.ОбработкаЗавершена = пно_Модуль.пнобсп_КонфигурацияИнтерфейс()
		.ПроцессДанныеОбработкаЗавершена(Процесс.Очередь, ТипИмя);
	КонецЕсли;
	
КонецПроцедуры // ЗавершенУстановить 

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

#КонецОбласти

// BSLLS:NestedFunctionInParameters-on
// BSLLS:EmptyRegion-on
 