﻿// Описание обработчика жтапа обновления компонента подсистемы.
//
// BSLLS:NestedFunctionInParameters-off - без лишних переменных выполнение быстрее
// BSLLS:EmptyRegion-off - пустые области разрешены

#Область ПрограммныйИнтерфейс

// Возвращает новый обработчик этапа обновления компонента подсистемы
//
// Параметры:
// 	Этап - Структура - Этап обновления компонента
// 	Метод - Строка - Полное имя метода общего модуля или модуля менеджера
//
// Возвращаемое значение:
// 	Структура
//
Функция Добавить(Этап, Метод) Экспорт
	
	Результат	= Новый Структура();
	
	Метод(Результат, Метод);
	ВыполнениеРежим(Результат, пно_ОбработчикВыполнениеРежим.Умолчание());
	ЗаполнениеЭто(Результат, Ложь);
	Комментарий(Результат, "");
	Идентификатор(Результат, Неопределено);
	ПереходВыполнениеОтключить(Результат, Ложь);
	пно_Изменение.Создать(Результат);
	
	пно_Этап.Обработчики(Этап).Добавить(Результат);
	
	Возврат Результат;
	
КонецФункции // Добавить 

// Устанавливает выполняемый метод обработчика. 
//
// Параметры:
// 	Обработчик - Структура - Описание обработчика
// 	Значение - Строка - Полное имя метода
//
// Возвращаемое значение:
// 	Строка
//
Функция Метод(Обработчик, Значение = Null) Экспорт
	
	Возврат пно_Коллекция.СтруктураСвойство(Обработчик, "Метод", Значение);
	
КонецФункции // Метод 

// Устанавливает режим выполнения обработчика. Возвращаем значение на момент до полнения метода
// 	см. [пно_ОбрабочикВыполнениеРежим](./пно_КомпонентыОбновленияПодсистем.пно_ОбработчикВыполнениеРежим.html)
//
// Параметры:
// 	Обработчик - Структура - Модифицируемый обработчик
// 	Значение - Строка - Режим выполнения
//
// Возвращаемое значение:
// 	Строка
//
Функция ВыполнениеРежим(Обработчик, Значение = Null) Экспорт
	
	Возврат пно_Коллекция.СтруктураСвойство(Обработчик, "ВыполнениеРежим", Значение);
	
КонецФункции // ВыполнениеРежим 

// Устанавливает признак обработчика для первоначального заполнения данных. Возвращает
// 	значение на момент до выполнения метода
//
// Параметры:
// 	Обработчик - Структура - Модфицируемый обрабочик
// 	Значение - Булево - Новое значение
//
// Возвращаемое значение:
// 	Булево
//
Функция ЗаполнениеЭто(Обработчик, Значение = Null) Экспорт
	
	Возврат пно_Коллекция.СтруктураСвойство(Обработчик, "ЗаполнениеЭто", Значение);
	
КонецФункции // ЗаполнениеЭто 

// Устанавливает комментарий описания действий, выполняемых обработчиком. Возвращает значение 
// 	на момент до выполнения метода  
// > Значимо для обработчиков режима выполнения Отложено
//
// Параметры:
// 	Обработчик - Структура - Модифицируемый обработчик
// 	Значение - Строка - Новое значение
//
// Возвращаемое значение:
// 	Строка
//
Функция Комментарий(Обработчик, Значение = Null) Экспорт
	
	Возврат пно_Коллекция.СтруктураСвойство(Обработчик, "Комментарий", Значение);
	
КонецФункции // Комментарий 

// Устанавливает УникальныйИдентификатор обработчика. Возвращает значение на 
// 	момент до выполнения метода.
// > Значимо для обработчиков режима выполнения Отложено  
// > в БСП требуется для идентификации обработчика в случае его переименования
//
// Параметры:
// 	Обработчик - Структура - Модифицируемый обработчик
// 	Значение - УникальныйИдентификатор - Новое значение
//
// Возвращаемое значение:
// 	УникальныйИдентификатор
//
Функция Идентификатор(Обработчик, Значение = Null) Экспорт
	
	Возврат пно_Коллекция.СтруктураСвойство(Обработчик, "Идентификатор", Значение);
	
КонецФункции // Идентификатор 

// Устанавливает сведения об изменении данных обработчиком. Возвращает значение на
// 	момент до выполнения метода
//
// Параметры:
// 	Обработчик - Структура - Модифицируемый обработчик
// 	Значение - Структура - Новое значение
//
// Возвращаемое значение:
// 	Структура
//
Функция Изменение(Обработчик, Значение = Null) Экспорт
	
	Возврат пно_Коллекция.СтруктураСвойство(Обработчик, "Изменение", Значение);
	
КонецФункции // Изменение 

// Устанавливает флаг отключения обработчика при переходе из другой программы. Вохвращает
// 	значение на момент до выполнения метода
//
// Параметры:
// 	Обработчик - Структура - Модифицируемый обработчик
// 	Значение - Булево - Новое значение
//
// Возвращаемое значение:
// 	Булево
//
Функция ПереходВыполнениеОтключить(Обработчик, Значение = Null) Экспорт
	
	Возврат пно_Коллекция.СтруктураСвойство(Обработчик, "МиграцияВыполнениеОтключить", Значение);
	
КонецФункции // ПереходВыполнениеОтключить 

#КонецОбласти

#Область СлужебныйПрограммныйИнтерфейс

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

#КонецОбласти
